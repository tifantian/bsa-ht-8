import CartParser from './CartParser';
import { exportAllDeclaration, qualifiedTypeIdentifier } from '@babel/types';
import v4 from 'uuid';
import fs, { unwatchFile } from 'fs';

let parser, validate, parseLine, calcTotal, parse;
const mockID = 'uuid';
jest.mock('fs');
jest.mock('uuid');

beforeEach(() => {
	parser = new CartParser();

	validate = parser.validate.bind(parser);
	parseLine = parser.parseLine.bind(parser);
	calcTotal = parser.calcTotal.bind(parser);
	parse = parser.parse.bind(parser);
});

beforeAll(() => {
	v4.mockReturnValue(mockID);
});

describe('CartParser - unit tests', () => {

	describe('validate', () => {

		// 1
		it('should return empty array validationErrors when content is correct', () => {
			const content =
				`Product name,Price,Quantity
				Mollis consequat,90,2
				Mollis ,102,1`;

			const validationErrors = validate(content);

			expect(validationErrors).toEqual([]);
		});

		// 2
		it('should return an error of type "header" when headers name is not correct', () => {
			const wrongHeaderName = 'Product';
			const content =
				`${wrongHeaderName}, Price, Quantity`;
			const realHeaderName = parser.schema.columns[0].name;
			const expectedValidationErrors =
				[{
					type: parser.ErrorType.HEADER,
					row: 0,
					column: 0,
					message: `Expected header to be named "${realHeaderName}" but received ${wrongHeaderName}.`
				}];

			const validationErrors = validate(content);

			expect(validationErrors).toEqual(expectedValidationErrors);
		});

		// 3
		it('should return an error of type "row" when rows length is not equal to schema', () => {
			const content =
				`Product name,Price,Quantity
				Mollis consequat,90,2
				Tvoluptatem,1`;
			const expectedLength = parser.schema.columns.length;
			const expectedValidationErrors =
				[{
					column: -1,
					message: `Expected row to have ${expectedLength} cells but received 2.`,
					row: 2,
					type: parser.ErrorType.ROW,
				}];

			const validationErrors = validate(content);

			expect(validationErrors).toEqual(expectedValidationErrors);
		});

		// 4
		it('should return an error of type "ceil" when someone number value less then 0', () => {
			const negativePrice = -13;
			const content =
				`Product name,Price,Quantity
				Mollis consequat,${negativePrice},2`;
			const expectedValidationErrors =
				[{
					column: 1,
					message: `Expected cell to be a positive number but received \"${negativePrice}\".`,
					row: 1,
					type: parser.ErrorType.CELL
				}];

			const validationErrors = validate(content);

			expect(validationErrors).toEqual(expectedValidationErrors);
		});

		// 5
		it('should return an error of type "ceil" when someone numbers ceil is a string', () => {
			const stringValue = "two dollars";
			const content =
				`Product name,Price,Quantity,
				 Iphone ,90 , ${stringValue}`;
			const expectedValidationErrors =
				[{
					column: 2,
					message: `Expected cell to be a positive number but received \"${stringValue}\".`,
					row: 1,
					type: parser.ErrorType.CELL
				}];

			const validationErrors = validate(content);

			expect(validationErrors).toEqual(expectedValidationErrors);
		});

		// 6	
		it('should return every error in array', () => {
			const content =
				`Poduct name,Price,Quantity
				Mollis consequat,-90,2
				Tvoluptatem`; // header, row, ceil errors

			const validationErrors = validate(content);

			expect(validationErrors.length).toBe(3);
		});

		// 7
		it('should return an error of type "ceil" when some ceil is an empty string', () => {
			const emptyValue = "";
			const content =
				`Product name,Price,Quantity,
				${emptyValue} ,90 , 312`;
			const expectedValidationErrors =
				[{
					column: 0,
					message: `Expected cell to be a nonempty string but received \"${emptyValue}\".`,
					row: 1,
					type: parser.ErrorType.CELL
				}];

			const validationErrors = validate(content);

			expect(validationErrors).toEqual(expectedValidationErrors);
		});

	});

	describe('parse', () => {

		// 8
		it('should parse when data is correct', () => {
			const price = 1000;
			const quantity = 15;
			const productName = "shawarma";
			const totalPrice = price * quantity;
			const contents =
				`Product name,Price,Quantity
				 ${productName},${price},${quantity}`;
			fs.readFileSync.mockReturnValueOnce(contents);
			const
				expectedValue = {
					items: [
						{
							id: mockID,
							name: productName,
							price,
							quantity
						}
					],
					total: totalPrice
				}

			const returnValue = parse();

			expect(returnValue).toEqual(expectedValue);
		});

	});

	describe('parseLine', () => {

		// 9	
		it('should return correct parse Line', () => {
			const
				name = 'shawarma',
				price = 10000000,
				quantity = 1;
			const
				expectedObject = {
					id: mockID,
					name,
					price,
					quantity
				}

			const item = parseLine(`${name}, ${price}, ${quantity}`);

			expect(item).toEqual(expectedObject);
		});

		// 10
		test('should throw error when received is not correct data', () => {
			const content = `Product name, Quantity`;
			fs.readFileSync.mockReturnValueOnce(content);

			expect(parse).toThrow('Validation failed!');
		});

	});

	describe('calcTotal', () => {

		// 11
		it('should return correct total price value', () => {
			const
				price_1 = 30,
				quantity_1 = 2,
				price_2 = 90,
				quantity_2 = 7;
			const objectCartItems =
				[
					{
						price: price_1,
						quantity: quantity_1
					},
					{
						price: price_2,
						quantity: quantity_2
					}
				];
			const sumResult = quantity_1 * price_1 + quantity_2 * price_2;

			const totalPrice = calcTotal(objectCartItems);

			expect(totalPrice).toBe(sumResult);
		});

	});

});

describe('CartParser - integration test', () => {

	// 12
	it('should return expected result', () => {
		const csv = './samples/cart.csv';
		const contents =
		`Product name,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1
		Consectetur adipiscing,28.72,10
		Condimentum aliquet,13.90,1`;
		const expectedObject = {
			"items": [
				{
					"id": mockID,
					"name": "Mollis consequat",
					"price": 9,
					"quantity": 2
				},
				{
					"id": mockID,
					"name": "Tvoluptatem",
					"price": 10.32,
					"quantity": 1
				},
				{
					"id": mockID,
					"name": "Scelerisque lacinia",
					"price": 18.9,
					"quantity": 1
				},
				{
					"id": mockID,
					"name": "Consectetur adipiscing",
					"price": 28.72,
					"quantity": 10
				},
				{
					"id": mockID,
					"name": "Condimentum aliquet",
					"price": 13.9,
					"quantity": 1
				}
			],
			"total": 348.32
		}
		fs.readFileSync.mockReturnValueOnce(contents);
		
		expect(parse(csv)).toEqual(expectedObject);
	});

});